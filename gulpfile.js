var gulp = require('gulp');
var sass = require('gulp-ruby-sass');
var autoprefix = require('gulp-autoprefixer');
var livereload = require('gulp-livereload');


//SASS
gulp.task('styles', function () {
    return sass('assets/sass/main.scss').pipe(gulp.dest('assets/css/'));
});
//Autoprefixer
gulp.task('autopref', function () {
    gulp.src('assets/sass/main.scss').pipe(autoprefix()).pipe(gulp.dest('assets/css/'));
});
//
//Watch
gulp.task('watch', function () {gulp.watch('assets/sass/*.scss', ['autopref']);
    gulp.watch('assets/sass/**/*.scss', ['autopref']);
});
gulp.task('default', ['styles', 'watch']);
// Live Reload
gulp.task('watch', function () {
    livereload.listen();
    gulp.watch('assets/sass/**/*.scss', ['styles']);
});
